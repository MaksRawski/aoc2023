fn main() {
    let mut ans: u32 = 0;
    let s = std::fs::read_to_string("input").unwrap();
    for l in s.lines(){
        ans += solution(l) as u32;
    }
    println!("{}", ans);
}

fn solution(s: &str) -> u8{
    let mut first: u8 = 255;
    let mut last: u8 = 255;
    for c in s.chars(){
        if let Ok(d) = c.to_string().parse::<u8>(){
            if first == 255 {
                first = d;
                last = first;
            } else {
                last = d;
            }
        }
    }
    first * 10 + last
}

#[test]
fn test_01() {
    let test_data = vec!["1abc2", "pqr3stu8vwx", "a1b2c3d4e5f", "treb7uchet"];
    let ans = vec![12, 38, 15, 77];
    for (i, t) in test_data.iter().enumerate(){
        assert_eq!(solution(t), ans[i]);
    }
}
