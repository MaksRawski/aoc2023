#include <cctype>
#include <cstdint>
#include <fstream>
#include <string>
#include <iostream>

const std::string NUMBERS[] {
    "zero", "one", "two",   "three", "four",
    "five", "six", "seven", "eight", "nine",
};

int number(const std::string &s){
    for (int i = 0; i < 10; ++i){
        if (NUMBERS[i] == s){
            return i;
        }
    }
    return 255;
}

int solution(const std::string &s){
    int first = 255;
    int last = 255;
    for (int i = 0; i < s.length(); ++i) {
        // std::cout << "s[" << i << "]: " << s[i] << std::endl;
        if (s[i] <= '9' && s[i] >= '0'){
            if (first == 255){
                first = s[i] - '0';
                last = s[i] - '0';
            } else {
                last = s[i] - '0';
            }
            // std::cout << "[D] f:" << first << " l:" << last << std::endl;
        }
        for (int len = 3; len < 6 && i + len <= s.length(); ++len){
            int n = number(s.substr(i, len));
            // std::cout << s.substr(i, len) << " number: " << n << std::endl;
            if (n != 255) {
                if (first == 255) {
                  first = n;
                  last = first;
                } else {
                  last = n;
                }
                // std::cout << "[L] f:" << first << " l:" << last << std::endl;
            }
        }
        // std::cout << "f:" << first << " l:" << last << std::endl;
    }
    return first * 10 + last;
}

#include <assert.h>
void test(){
    assert(solution("two1nine") == 29);
    assert(solution("eightwothree") == 83);
    assert(solution("abcone2threexyz") == 13);
    assert(solution("xtwone3four") == 24 );
    assert(solution("4nineeightseven2") == 42);
    assert(solution("zoneight234") == 14);
    assert(solution("7pqrstsixteen") == 76);
}

int main(){
    std::ifstream in ("input");
    std::string line;
    int sum = 0;
    while (std::getline(in, line)) {
        sum += solution(line);
    }
    std::cout << sum << std::endl;
}
