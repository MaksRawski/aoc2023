#include <iostream>
#include <list>
#include <array>
#include <string>
#include <algorithm>
#include <fstream>


inline bool is_symbol(const char c){
	return !isdigit(c) && c != '.';
}

std::list<int> find_parts_numbers(const std::array<std::string, 2> &batch) {
    std::list<int> parts;
    int batch_length = batch[0].size();
    int number0 = 0, number1 = 0;
	bool num0_is_part_number = false, num1_is_part_number = false;

    for (int i = 0; i < batch_length; ++i) {
		// ROW 0
        if (isdigit(batch[0][i])) {
            number0 = number0 * 10 + batch[0][i] - '0';
        } else if (batch[0][i] != '.'){
			// if the current char is neither a digit nor a dot it means that it's
			// a symbol around which any number will end up being a part number

			if (number0 != 0){
				// if we had a number before current char then it was a part number
				parts.push_back(number0);
				number0 = 0;
				num0_is_part_number = false;
			}
			if (i + 1 < batch_length && isdigit(batch[0][i + 1])){
				// if next char is a digit then that number is a part number
				num0_is_part_number = true;
			}
			if ((i > 0 && isdigit(batch[1][i-1])) || isdigit(batch[1][i]) || (i + 1 < batch_length && isdigit(batch[1][i+1]))){
				// if there is a number nearby in the row below
				num1_is_part_number = true;
			}
		} else if (i > 0 && isdigit(batch[0][i-1])){
			// if current number is not a digit (nor a symbol) but previous was
			// then it's the end of that number
			if (num0_is_part_number || is_symbol(batch[1][i])) {
				// we also check whether there is a symbol on the diagonal of the now ended number
				// that is this situation:
				// 123...
				// ...*..
				parts.push_back(number0);
				num0_is_part_number = false;
			}
			number0 = 0;
		}

		// ROW 1
		if (isdigit(batch[1][i])) {
            number1 = number1 * 10 + batch[1][i] - '0';
        } else if (batch[1][i] != '.'){
			// if the current char is neither a digit nor a dot it means that it's
			// a symbol around which any number will end up being a part number

			if (number1 != 0){
				// if we had a number before current char then it was a part number
				parts.push_back(number1);
				number1 = 0;
				num1_is_part_number = false;
			}
			if (i + 1 < batch_length && isdigit(batch[1][i + 1])){
				// if next char is a number then it will be a part number
				num1_is_part_number = true;
			}
			if (isdigit(batch[0][i]) || (i + 0 < batch_length && isdigit(batch[0][i+1]))){
				// if there is a number in row above
				// NOTE: we don't have to check for the diagonal situation as described in ROW 0
				// as it's already handled there; this situation:
				// 123...
				// ...*..
				num0_is_part_number = true;
			}
		} else if (i > 0 && isdigit(batch[1][i-1])){
			// if current number is not a digit (nor a symbol) but previous was then
			// it's the end of that number
			if (num1_is_part_number) {
				parts.push_back(number1);
				num1_is_part_number = false;
			}
			if (i+1 < batch_length && isdigit(batch[1][i+1])) {
				// if next char is a digit then that number will be a part number
				num1_is_part_number = true;
			}
			number1 = 0;
		}
    }
	if (number0 != 0 && num0_is_part_number) parts.push_back(number0);
	if (number1 != 0 && num1_is_part_number) parts.push_back(number1);
    return parts;
}

#include "../assert_eq.h"
void test_fpn_helper(const std::array<std::string, 2> &batch, const std::list<int> expected_output){
	std::list<int> parts = find_parts_numbers(batch);
	ASSERT_EQF(parts, expected_output, [batch]{std::cout << "batch: [\"" << batch[0] << "\", \"" << batch[1] << "\"]" << std::endl; });
}
void tests_find_parts_numbers() {
	// test symbol nearby
	test_fpn_helper({"!123", "...."}, {123});
	test_fpn_helper({"123!", "...."}, {123});
	// test symbol under
	test_fpn_helper({"456.", "@..."}, {456});
	test_fpn_helper({"456.", "..@."}, {456});
	test_fpn_helper({"456.", "...@"}, {456});
	test_fpn_helper({".789", "...#"}, {789});
	test_fpn_helper({".789", ".#.."}, {789});
	test_fpn_helper({".789", "#..."}, {789});
	// test symbol over
	test_fpn_helper({"@...", "456."}, {456});
	test_fpn_helper({"..@.", "456."}, {456});
	test_fpn_helper({"...@", "456."}, {456});
	test_fpn_helper({"...#", ".789"}, {789});
	test_fpn_helper({".#..", ".789"}, {789});
	test_fpn_helper({"#...", ".789"}, {789});
	// test symbol too far
	test_fpn_helper({"$.100.", "$....."}, {});
	test_fpn_helper({"100...", "....$."}, {});
	// test one symbol multiple parts
	test_fpn_helper({"10....", "%11..."}, {10, 11});
	test_fpn_helper({".10^12", ".11..."}, {10, 11, 12});
	test_fpn_helper({".10^12", ".11.13"}, {10, 11, 12, 13});

	std::cout << "test_find_parts_numbers " << green_fg("passed!") << std::endl;
}


int part1(const std::string input_filename) {
    std::ifstream input(input_filename);
	std::array<std::string, 2> batch;
	std::list<int> part_numbers;
	std::list<int> parts_in_previous_batch;
	int sum = 0;

	short int i = 1;
	std::string line;
	getline(input, line);
	batch[0] = line;

	for (; getline(input, line); ++i){
		batch[i%2] = line;
		std::list<int> parts_in_batch = find_parts_numbers(batch);

		// std::cout << "batch: [\"" << batch[0] << "\", \"" << batch[1] << "\"]" << std::endl;
		for (auto part_number : parts_in_batch){
			// check if this part wasn't in previous batch to avoid counting the same part twice
			if (std::find(parts_in_previous_batch.begin(), parts_in_previous_batch.end(), part_number) == parts_in_previous_batch.end()) {
				part_numbers.push_back(part_number);
				sum += part_number;
			}
		}
		parts_in_previous_batch.swap(parts_in_batch);
	}
	return sum;
}

void test_part1() {
	ASSERT_EQ(part1("test"), 4361);
	std::cout << "test_part1 " << green_fg("passed!") << std::endl;
}

int main() {
	tests_find_parts_numbers();
	test_part1();
	// AAAAAAAAAAAAA
	// TODO: too high!
	std::cout << "part1 answer: " << part1("input") << std::endl;;
}
