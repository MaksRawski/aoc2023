#ifndef ASSERT_EQ_H_
#define ASSERT_EQ_H_

#include <assert.h>
#include <iostream>

inline const std::string red_fg(const std::string &s){
    return "\x1b[1;31m" + s + "\x1b[0m";
}
inline const std::string green_fg(const std::string &s){
    return "\x1b[1;32m" + s + "\x1b[0m";
}

static inline void assert_eq(int x, int y){
	if (x != y) {
		std::cout << x << " != " << y << std::endl;
	}
}

#include <list>
template<typename T>
void assert_eq(std::list<T> x, std::list<T> y){
	if (x != y) {
		if (x.empty()) std::cout << "<EMPTY>";
		for (auto i : x) {
			std::cout << i << " ";
		}
		std::cout << " != ";

		if (y.empty()) std::cout << "<EMPTY>";
		for (auto i : y) {
			std::cout << i << " ";
		}
		std::cout << std::endl;
	}
}

#define ASSERT_EQ(x, y) do { \
	if (x != y) { \
		std::cout << "Assertion " << red_fg("failed") << " at line " << __LINE__ << ":" << std::endl; \
		assert_eq(x, y); \
		exit(1); \
	} \
} while (false);

#define ASSERT_EQF(x, y, f) do { \
	if (x != y) { f();  assert_eq(x, y);  exit(1); } \
} while (false);

#endif // ASSERT_EQ_H_
