#include <iostream>
#include <string>
#include <assert.h>
#include "../assert_eq.h"

const int MAX_RED = 12;
const int MAX_GREEN = 13;
const int MAX_BLUE = 14;

class CubeSet{
  public:
    int red, green, blue;
    CubeSet(std::string cube_set);
    CubeSet(int red, int green, int blue): red(red), green(green), blue(blue){};
    bool isvalid();
    int power();
    friend std::ostream& operator<<(std::ostream &out, const CubeSet &cs);
    friend void test_cubeset();
};

std::ostream& operator<<(std::ostream &out, const CubeSet &cs){
    return out << "red: " << cs.red << ", green: " << cs.green << ", blue: " << cs.blue;
}

inline bool isdigit(const char &c){
    return c >= '0' && c <= '9';
}

void test_isdigit(){
    assert(isdigit('5'));
    assert(!isdigit('a'));
    assert(isdigit('0'));
    assert(!isdigit('0' - 1));
    assert(isdigit('9'));
    assert(!isdigit('9' + 1));
    std::cout << "test_isdigit " << green_fg("passed!") << std::endl;
}

CubeSet::CubeSet(const std::string s): red(0), green(0), blue(0){
    int nbeg;
    int nlen = 0;
    int n;

    for (int i = 0; i < s.length(); ++i){
        if (isdigit(s[i])) {
            nbeg = i;
            while (isdigit(s[i])) ++nlen, ++i;
            n = std::atoi(s.substr(nbeg, nlen).c_str());
        }
        nbeg = i;
        nlen = 0;
        assert(s[i++] == ' ');
        if (s.substr(i, 3) == "red") red = n;
        else if (s.substr(i, 4) == "blue") blue = n;
        else if (s.substr(i, 5) == "green") green = n;
        while(s[i] != '\0' && s[i] != ',') ++i;
        ++i;
    }
}

int CubeSet::power(){
    return red * green * blue;
}

bool CubeSet::isvalid(){
    return red <= MAX_RED && green <= MAX_GREEN && blue <= MAX_BLUE;
}




void test_cubeset(){
    CubeSet cs1("1 red, 2 green, 3 blue");
    assert_eq(cs1.red, 1);
    assert_eq(cs1.green, 2);
    assert_eq(cs1.blue, 3);
    assert(cs1.isvalid());

    CubeSet cs2("12 red, 14 blue, 13 green");
    assert_eq(cs2.red, 12);
    assert_eq(cs2.green, 13);
    assert_eq(cs2.blue, 14);
    assert(cs2.isvalid());

    CubeSet cs3("13 green, 12 red, 15 blue");
    assert_eq(cs3.red, 12);
    assert_eq(cs3.green, 13);
    assert_eq(cs3.blue, 15);
    assert(!cs3.isvalid());

    std::cout << "test_cubeset " <<  green_fg("passed!") << std::endl;
}

#include <vector>
class Game{
    std::vector<CubeSet> sets;
public:
    Game(std::string line);
    bool isvalid();
    CubeSet min_set();
    friend std::ostream& operator<<(std::ostream &out, const Game &game);
};

Game::Game(std::string line){
    int beg = 5;
    while (line[beg] != ':') ++beg;
    beg += 2;

    for (int i = beg; i < line.length(); ++i){
        if ((line[i] == ';' && ++i) || i == line.length() - 1){
            sets.push_back(CubeSet(line.substr(beg, i - beg + 1)));
            beg = i+1;
        }
    }
}

std::ostream& operator<<(std::ostream &out, const Game &game){
    for (CubeSet cs : game.sets){
        out << "{" << cs << "} ";
    }
    return out;
}

bool Game::isvalid(){
    bool res = true;
    for (CubeSet s : sets){
        if (!s.isvalid()) res = false;
    }
    return res;
}

CubeSet Game::min_set(){
    int max_red = 0, max_green = 0, max_blue = 0;
    for (CubeSet cs : sets){
        if (cs.red > max_red) max_red = cs.red;
        if (cs.green > max_green) max_green = cs.green;
        if (cs.blue > max_blue) max_blue = cs.blue;
    }
    return CubeSet(max_red, max_green, max_blue);
}

#include <fstream>
void test_part1() {
    Game t1 ("Game 1: 1 red, 2 green, 3 blue; 2 red, 3 blue, 4 green");
    assert(t1.isvalid());

    Game t2 ("Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green");
    assert(t2.isvalid());

    Game t3 ("Game 123: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red");
    assert(!t3.isvalid());

    std::ifstream input ("test");
    std::string line;

    for (int i = 0; getline(input, line); ++i) {
        Game g(line);
        if (i == 2 || i == 3) assert(!g.isvalid());
        else assert(g.isvalid());
    }
    std::cout << "test_part1 " <<  green_fg("passed!") << std::endl;
}

void part1(){
    std::ifstream input ("input");
    std::string line;
    int sum = 0;

    for (int i = 0; getline(input, line); ++i) {
        Game g(line);
        if (g.isvalid()) sum += i + 1;
    }
    std::cout << "part 1 answer: " << sum << std::endl;
}


void test_part2(){
    std::ifstream input ("test");
    std::string line;
    int expected[5] = {48, 12, 1560, 630, 36};

    for (int i = 0; getline(input, line); ++i) {
        Game g(line);
        assert_eq(g.min_set().power(), expected[i]);
    }
    std::cout << "test_part2 " << green_fg("passed!") << std::endl;
}

void part2(){
    std::ifstream input ("input");
    std::string line;
    int sum = 0;

    while (getline(input, line)) {
        Game g(line);
        sum += g.min_set().power();
    }
    std::cout << "part 2 answer: " << sum << std::endl;
}

int main(){
    test_isdigit();
    test_cubeset();
    test_part1();
    test_part2();
    part1();
    part2();
}
